//
//  Task+CoreDataProperties.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 13/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//
//

import Foundation
import CoreData


extension Task {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Task> {
        return NSFetchRequest<Task>(entityName: "Task")
    }

    @NSManaged public var name: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var desc: String?
    @NSManaged public var done: Bool
    @NSManaged public var list: List?

}
