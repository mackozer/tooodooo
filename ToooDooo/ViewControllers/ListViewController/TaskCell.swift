//
//  TaskCell.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 14/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import UIKit

protocol TaskCellDelegate: class {
    func checkImageTapped(for index: Int)
}

class TaskCell: UITableViewCell {
    
    weak var taskCellDelegate: TaskCellDelegate?
    
    @IBOutlet var checkImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(checkImageTapped))
        checkImage.addGestureRecognizer(recognizer)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func checkImageTapped() {
        print("Image tapped")
        guard let index = index() else { return }
        taskCellDelegate?.checkImageTapped(for: index)
    }
    
    func index() -> Int? {
        guard let superView = self.superview as? UITableView else { return nil }
        return superView.indexPath(for: self)?.row
    }

}
