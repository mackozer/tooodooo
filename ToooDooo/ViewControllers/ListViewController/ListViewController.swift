//
//  ListViewController.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 14/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import UIKit
import CoreData

class ListViewController: UITableViewController {
    
    var managedContext: NSManagedObjectContext!
    
    var list: List!
    var dataSource: ListViewDataSource!
    var selectedTask: Task?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTask))
        title = list.name
        dataSource = ListViewDataSource(list: list, managedContext: managedContext, controller: self)
        tableView.dataSource = dataSource
        tableView.tableFooterView = UIView()
        
        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: Strings.ListView.clearDoneTasks)
        refreshControl.addTarget(self, action: #selector(clearDoneTaks), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataSource.sortTasks()
    }
    
    @objc func addTask() {
        performSegue(withIdentifier: StringLiterals.Identifiers.addTaskView, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == StringLiterals.Identifiers.addTaskView {
            if let controller = segue.destination as? AddTaskViewController {
                controller.managedContext = managedContext
                controller.list = list
                controller.state = .add
                controller.addTaskDelegate = self
            }
        } else if segue.identifier == StringLiterals.Identifiers.editTask {
            guard let selectedTask = selectedTask else { return }
            if let controller = segue.destination as? AddTaskViewController {
                controller.managedContext = managedContext
                controller.name = selectedTask.name
                controller.details = selectedTask.desc
                controller.state = .edit
                controller.task = selectedTask
                controller.addTaskDelegate = self
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let task = dataSource.selectTask(at: indexPath.row) else { return }
        selectedTask = task
        performSegue(withIdentifier: StringLiterals.Identifiers.editTask, sender: nil)
    }
    
    @objc func clearDoneTaks() {
        dataSource.clearDoneTasks { (successs) in
            
            if successs {
                refreshControl?.endRefreshing()
                tableView.reloadData()
            } else {
                UIAlertController.errorMessage(title: Strings.ListView.saveErrorTitle, message: Strings.ListView.saveErrorMessage, target: self)
            }
        }
    }
}

extension ListViewController: TaskCellDelegate {
    func checkImageTapped(for index: Int) {
        let task = dataSource.tasks[index]
        task.done.toggle()
        CoreDataManager.save(managedContext: managedContext) { (success) in
            refreshControl?.endRefreshing()
            if success {
                dataSource.sortTasks()
                UIView.transition(with: tableView, duration: 0.2, options: .transitionCrossDissolve, animations: {
                    self.tableView.reloadData()
                }, completion:  nil)
            }
        }
    }
}

extension ListViewController: AddTaskViewControlerDelegate {
    func refresh() {
        managedContext.refresh(list, mergeChanges: false)
        dataSource.tasks.removeAll()
        dataSource.tasks = list.tasks?.array as? [Task] ?? []
        tableView.reloadData()
    }
}
