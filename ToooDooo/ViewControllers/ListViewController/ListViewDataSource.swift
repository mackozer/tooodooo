//
//  ListViewDataSource.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 14/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import UIKit
import CoreData

class ListViewDataSource: NSObject, UITableViewDataSource {
    
    let list: List
    var tasks: [Task]
    var controller: ListViewController
    
    var managedContext: NSManagedObjectContext
    
    init(list: List, managedContext: NSManagedObjectContext, controller: ListViewController) {
        self.list = list
        self.managedContext = managedContext
        self.controller = controller
        tasks = list.tasks?.array as? [Task] ?? []
        

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: StringLiterals.Identifiers.cell, for: indexPath) as! TaskCell
        let task = tasks[indexPath.row]
        cell.titleLabel.text = task.name
        cell.descriptionLabel.text = task.desc
        cell.checkImage.image = task.done == true ? UIImage(named: StringLiterals.Names.checked) : UIImage(named: StringLiterals.Names.unchecked)
        cell.taskCellDelegate = controller
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let taskToDelete = tasks[indexPath.row]
            managedContext.delete(taskToDelete)
            CoreDataManager.save(managedContext: managedContext) { (success) in
                if success {
                    tasks.remove(at: indexPath.row)
                    let indexPaths = [indexPath]
                    tableView.deleteRows(at: indexPaths, with: .automatic)
                }
            }
        }
    }
    
    func selectTask(at index: Int) -> Task? {
        return tasks[index]
    }
    
    func sortTasks() {
        tasks.sort { !$0.done && $1.done }
    }
    
    func clearDoneTasks(completion: (Bool) -> Void) {
        
        let undoneTasks = tasks.filter { $0.done == false }
        let doneTasks = tasks.filter { $0.done == true }
        for doneTask in doneTasks {
            managedContext.delete(doneTask)
        }
        
        CoreDataManager.save(managedContext: managedContext) { (success) in
            if success {
                tasks.removeAll()
                tasks = undoneTasks
            }
            completion(success)
        }
    }
}


