//
//  AddTaskViewController.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 14/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import UIKit
import CoreData

enum AddTaskViewState {
    case add, edit
}

protocol AddTaskViewControlerDelegate: class {
    func refresh()
}

class AddTaskViewController: UIViewController {
    
    weak var addTaskDelegate: AddTaskViewControlerDelegate?
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var detailsLabel: UILabel!
    @IBOutlet var detailsTextField: UITextField!
    
    var managedContext: NSManagedObjectContext!
    
    var list: List?
    var task: Task?
    var state: AddTaskViewState!
    var name: String!
    var details: String!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(saveTapped))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if state == AddTaskViewState.edit {
            nameTextField.text = name
            detailsTextField.text = details
            title = Strings.AddTaskView.editTitle
        } else {
            title = Strings.AddTaskView.addTitle
        }
    }
    
    @objc func saveTapped() {
        if state == AddTaskViewState.add {
            addTask()
        } else {
            update()
        }
    }
    
    func addTask() {
        guard let list = list else { return }
        CoreDataManager.addTask(name: nameTextField.text ?? "", description: detailsTextField.text ?? "", date: NSDate(), to: list, for: managedContext) { (success) in
            if success {
                navigationController?.popViewController(animated: true)
                addTaskDelegate?.refresh()
            } else {
                UIAlertController.errorMessage(title: Strings.AddTaskView.saveErorrTitle, message: Strings.AddTaskView.saveErrorMessage, target: self)
            }
        }
    }
    
    func update() {
        guard let task = task else { return }
        task.name = nameTextField.text
        task.desc = detailsTextField.text
        CoreDataManager.save(managedContext: managedContext) { (success) in
            if success {
                navigationController?.popViewController(animated: true)
                addTaskDelegate?.refresh()
            } else {
                UIAlertController.errorMessage(title: Strings.AddTaskView.saveErorrTitle, message: Strings.AddTaskView.saveErrorMessage, target: self)
            }
        }
    }

}
