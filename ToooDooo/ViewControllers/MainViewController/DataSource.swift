//
//  DataSource.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 13/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import UIKit
import CoreData

class DataSource: NSObject, UITableViewDataSource {
    
    var managedContext: NSManagedObjectContext!
    var mainViewController: MainViewController!
    var lists: [List]
    
    init(managedContext: NSManagedObjectContext, controller: MainViewController) {
        self.managedContext = managedContext
        self.mainViewController = controller
        lists = CoreDataManager.initialListSetup(for: managedContext) ?? []
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: StringLiterals.Identifiers.cell, for: indexPath)
        let list = lists[indexPath.row]
        let taskCount = list.pendingTasks() ?? 0
        cell.textLabel?.text = list.name
        cell.detailTextLabel?.text = "\(taskCount) pending tasks"
        cell.imageView?.image = UIImage(named: list.icon ?? StringLiterals.Names.defaultListIcon)
        return cell
    }
    
    func reload() {
        lists.removeAll()
        lists = CoreDataManager.fetchLists(for: managedContext)
    }
    
    func selectedList(for index: Int) -> List {
        return lists[index]
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let listToDelete = lists[indexPath.row]
            managedContext.delete(listToDelete)
            CoreDataManager.save(managedContext: managedContext) { (success) in
                
                if success {
                    lists.remove(at: indexPath.row)
                    let indexPaths = [indexPath]
                    tableView.deleteRows(at: indexPaths, with: .automatic)
                } else {
                    UIAlertController.errorMessage(title: Strings.MainView.deletingErrorTitle, message: Strings.MainView.deletingErrorMessage, target: mainViewController)
                }
            }
        }
    }
}
