//
//  ViewController.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 13/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import UIKit
import CoreData

class MainViewController: UITableViewController {
    
    var managedContext: NSManagedObjectContext!
    var dataSource: DataSource!
    var selectedList: List!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "ToooDooo"
        dataSource = DataSource(managedContext: managedContext, controller: self)
        tableView.dataSource = dataSource
        tableView.tableFooterView = UIView()
        tableView.reloadData()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addList))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dataSource.reload()
        tableView.reloadData()
    }
    
    @objc func addList() {
        performSegue(withIdentifier: StringLiterals.Identifiers.addListView, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == StringLiterals.Identifiers.addListView {
            if let addListVC = segue.destination as? AddListViewController {
                addListVC.managedContext = managedContext
            }
        } else if segue.identifier == StringLiterals.Identifiers.showList {
            if let listVC = segue.destination as? ListViewController {
                listVC.managedContext = managedContext
                listVC.list = selectedList
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedList = dataSource.selectedList(for: indexPath.row)
        performSegue(withIdentifier: StringLiterals.Identifiers.showList, sender: nil)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            dataSource.lists.remove(at: indexPath.row)
            CoreDataManager.save(managedContext: managedContext) { (success) in
                if success {
                    let indexPaths = [indexPath]
                    tableView.deleteRows(at: indexPaths, with: .automatic)
                } else {
                    UIAlertController.errorMessage(title: Strings.MainView.deletingErrorTitle, message: Strings.MainView.deletingErrorMessage, target: self)
                }
            }
        }
    }
}

