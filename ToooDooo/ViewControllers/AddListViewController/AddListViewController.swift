//
//  AddListViewController.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 13/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import UIKit
import CoreData

class AddListViewController: UIViewController {
    
    var managedContext: NSManagedObjectContext!

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var iconLabel: UILabel!
    @IBOutlet var iconPreview: UIImageView!
    
    var iconName: String! {
        didSet {
            iconPreview.image = UIImage(named: iconName)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
        nameLabel.text = Strings.AddListView.name
        iconLabel.text = Strings.AddListView.icon
        iconName = "\(StringLiterals.Names.listIcon)1"
        let recogniezer = UITapGestureRecognizer(target: self, action: #selector(showIconList(recognizer:)))
        iconPreview.addGestureRecognizer(recogniezer)
    }
    
    @objc func done() {
        CoreDataManager.addList(name: nameField.text ?? "", icon:  iconName, for: managedContext) { (success) in
            if success {
                navigationController?.popViewController(animated: true)
            } else {
                UIAlertController.errorMessage(title: Strings.AddListView.saveErrorTitle, message: Strings.AddListView.saveErrorMessage, target: self)
            }
        }
    }
    
    @objc func showIconList(recognizer: UITapGestureRecognizer) {
        performSegue(withIdentifier: StringLiterals.Identifiers.showIcons, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == StringLiterals.Identifiers.showIcons {
            if let controller = segue.destination as? ListIconsViewController {
                controller.delegate = self
            }
        }
    }
}

extension AddListViewController: ListIconsViewControllerDelegate {
    func setIcon(_ name: String) {
        iconName = name
    }
}
