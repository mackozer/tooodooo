//
//  ListIconsDataSource.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 14/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import UIKit

class ListIconsDataSource: NSObject, UICollectionViewDataSource {
    
    var icons: [String]!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return icons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StringLiterals.Identifiers.cell, for: indexPath) as! IconCollectionViewCell
        let image = UIImage(named: icons[indexPath.row])
        cell.imageView.image = image
        return cell
    }
    
    func iconName(for index: Int) -> String? {
        if index > icons.count { return nil }
        return icons[index]
    }
    
    
}
