//
//  ListIconsViewController.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 14/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import UIKit

protocol ListIconsViewControllerDelegate: class {
    func setIcon(_ name: String)
}

class ListIconsViewController: UIViewController {
    
    weak var delegate: ListIconsViewControllerDelegate?

    @IBOutlet var collectionView: UICollectionView!
    
    var iconsDataSource: ListIconsDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iconsDataSource = ListIconsDataSource()
        iconsDataSource.icons = icons()
        collectionView.dataSource = iconsDataSource
        collectionView.delegate = self
        collectionView.reloadData()
    }
    
    func icons() -> [String] {
        var iconsArray = [String]()
        
        for i in 1...14 {
            let iconName = "\(StringLiterals.Names.listIcon)\(i)"
            iconsArray.append(iconName)
        }
        
        return iconsArray
        
    }

}

extension ListIconsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let name = iconsDataSource.iconName(for: indexPath.row) else { return }
        delegate?.setIcon(name)
        dismiss(animated: true)
    }
}
