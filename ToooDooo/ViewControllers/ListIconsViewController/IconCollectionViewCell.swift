//
//  IconCollectionViewCell.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 14/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import UIKit

class IconCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    
}
