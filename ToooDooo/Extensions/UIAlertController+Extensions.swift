//
//  UIAlertController+Extensions.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 14/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import UIKit

extension UIAlertController {
    static func errorMessage(title: String, message: String, target: UIViewController) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        target.present(ac, animated: true)
    }
}
