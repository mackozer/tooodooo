//
//  Strings.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 13/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import Foundation

enum StringLiterals {
    
    enum Names {
        static let tutorial = "Tutorial"
        static let listIcon = "listIcon-"
        static let defaultListIcon = "listIcon-1"
        static let checked = "checked"
        static let unchecked = "unchecked"
    }
    
    enum Identifiers {
        static let cell = "Cell"
        static let addListView = "AddListView"
        static let showIcons = "ShowIcons"
        static let showList = "ShowList"
        static let addTaskView = "AddTaskView"
        static let editTask = "EditTask"
    }
    
    
}
