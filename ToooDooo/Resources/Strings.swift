//
//  Strings.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 14/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import Foundation

enum Strings {
    
    enum MainView {
        static let deletingErrorTitle = "main_view_deleting_error_title".localized()
        static let deletingErrorMessage = "main_view_deleting_errro_message".localized()
        static let pendingTasks = "main_view_pending_tasks".localized()
    }
    
    enum AddListView {
        static let name = "add_list_view_name".localized()
        static let icon = "add_list_view_icon".localized()
        static let saveErrorTitle = "add_list_view_saving_error_title".localized()
        static let saveErrorMessage = "add_list_view_saving_error_message".localized()
    }
    
    enum AddTaskView {
        static let name = "add_task_view_name".localized()
        static let details = "add_task_view_details".localized()
        static let saveErorrTitle = "add_task_view_saving_error_title".localized()
        static let saveErrorMessage = "add_task_view_saviing_error_message".localized()
        static let addTitle = "add_task_view_title_add".localized()
        static let editTitle = "add_task_view_title_edit".localized()
    }
    
    enum ListView {
        static let saveErrorTitle = "list_view_saving_error_title".localized()
        static let saveErrorMessage = "list_view_saving_error_message".localized()
        static let clearDoneTasks = "list_view_clear_done_tasks".localized()
    }
}
