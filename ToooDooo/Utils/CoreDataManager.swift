//
//  CoreDataManager.swift
//  ToooDooo
//
//  Created by Krystian Kozerawski on 13/07/2019.
//  Copyright © 2019 Krystian Kozerawski. All rights reserved.
//

import Foundation
import CoreData

fileprivate let listFetch: NSFetchRequest<List> = List.fetchRequest()
fileprivate let taskFetch: NSFetchRequest<Task> = Task.fetchRequest()

class CoreDataManager {
    
    static func initialListSetup(for managedContext: NSManagedObjectContext) -> [List]? {
        
        do {
            let results = try managedContext.fetch(listFetch)
            if results.count > 0 {
                return results
            } else {
                let tutorialList = List(context: managedContext)
                tutorialList.name = StringLiterals.Names.tutorial
                tutorialList.icon  = StringLiterals.Names.defaultListIcon
                try managedContext.save()
                return CoreDataManager.initialListSetup(for: managedContext)
            }
        } catch let error as NSError {
            print("Fetch error: \(error) description: \(error.userInfo)")
        }
        return nil
    }
    
    static private func fetchObjects<T: NSManagedObject>(fetchRequest: NSFetchRequest<T>, for managedContext: NSManagedObjectContext) -> [T] {
        do {
            let results = try managedContext.fetch(fetchRequest)
            return results
        } catch let error as NSError {
            print("Fetch error: \(error) description: \(error.userInfo)")
        }
        
        return []
    }
    
    static func save(managedContext: NSManagedObjectContext, completion: (Bool) -> Void) {
        do {
            try managedContext.save()
            completion(true)
        } catch let error as NSError {
            print("Save error: \(error) description: \(error.userInfo)")
            completion(false)
        }
    }
    
    static func addList(name: String, icon: String, for managedContext: NSManagedObjectContext, completion: (Bool) -> Void) {
        let newList = List(context: managedContext)
        newList.name = name
        newList.icon = icon
        CoreDataManager.save(managedContext: managedContext, completion: completion)
    }
    
    static func addTask(name: String, description: String, date: NSDate, to list: List, for managedContext: NSManagedObjectContext, completion: (Bool) -> Void) {
        let newTask = Task(context: managedContext)
        newTask.name = name
        newTask.desc = description
        newTask.date = date
        newTask.done = false
        newTask.list = list
        CoreDataManager.save(managedContext: managedContext, completion: completion)
    }
    
    static func fetchLists(for managedContext: NSManagedObjectContext) -> [List] {
        return CoreDataManager.fetchObjects(fetchRequest: listFetch, for: managedContext)
    }
    
    static func fetchTasks(for managedContext: NSManagedObjectContext) -> [Task] {
        return CoreDataManager.fetchObjects(fetchRequest: taskFetch, for: managedContext)
    }
}
